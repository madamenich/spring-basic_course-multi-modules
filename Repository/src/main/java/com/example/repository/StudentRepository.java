package com.example.repository;

import com.example.repository.Model.Student;
import com.example.repository.Provider.StudentProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StudentRepository {
    @SelectProvider(type = StudentProvider.class,method = "findAll")
    List<Student> findAll();
}
